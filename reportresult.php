<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="gambar/logom10.svg">
  <title>Mitra10</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <?php
  include 'koneksi.php';
  // include 'algoritma.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['userlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang Akses, login terlebih dahulu"); location.href="login.php"</script>';
  }

  $strSQL = "SELECT * FROM tb_user WHERE username='$_SESSION[userlogin]'";
  $query = mysqli_query ($koneksi, $strSQL) or die ("query salah");
  $row = mysqli_fetch_array($query);

  $sql1 = "SELECT * FROM sum_score";
  $qry1 = mysqli_query ($koneksi, $sql1) or die ("query 1 salah");

  $sql2 = "SELECT hasil FROM sum_score";
  $qry2 = mysqli_query ($koneksi, $sql2) or die ("query 2 salah");
  $no = 0;
  while ($row2 = mysqli_fetch_array($qry2)) {
    $no++;
    $total2[$no] = $row2['hasil'];
  }
  $totalsum = number_format((array_sum($total2) / $no),2,",",".");
  $sumpersen = number_format(((array_sum($total2) / $no) / 5 * 100),2,",",".");

  $qry2 = mysqli_query($koneksi, "SELECT * FROM tb_pelanggan WHERE kritik!=''")or die("Query 2 salah!");

  ?>

</head>
<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <?php echo $row["username"];?> 
            <i class="fas fa-user-alt"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <div class="dropdown-divider"></div>
            <a href="setting.php" class="dropdown-item">
              <i class="fas fa-cog mr-2"></i>
              <span class="float-right text-muted text-sm">Setting</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="logout.php" class="dropdown-item">
              <i class="fas fa-sign-out-alt mr-2"></i>
              <span class="float-right text-muted text-sm">Logout</span>
            </a>
          </div>
        </li>
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="reportresult.php" class="brand-link">
        <img src="gambar/logom10.svg" alt="AdminLTE Logo" class="brand-image elevation-3"
        style="opacity: .8">
        <span class="brand-text font-weight-light">Mitra10 Q-Big</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <?php
            $cek_foto = $row ['foto'];
            $tempat_foto = 'foto/'.$row['foto']; 
            if ($cek_foto) {
              echo "<img src='$tempat_foto' class='img-circle elevation-2' alt='User Image'>"; 
            }else{
              echo "<img src='foto/blank.png'></a>";
            }
            ?>
          </div>
          <div class="info">
            <a href="setting.php" class="d-block">
              <?php echo $row["nama"];?></a>
            </div>
          </div>

          <!-- Sidebar Menu -->
          <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
           <li class="nav-item">
            <a href="reportresult.php" class="nav-link active">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Report Result
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Manage User Id
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="adduserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add User Id</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="updateuserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Update User Id</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Manage Question
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="title.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Question Title</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="question.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Question</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="setting.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="lockscreen.php?username=<?php echo $_SESSION['userlogin']; ?>" class="nav-link">
              <font style="color: #ed1c24;"><i class="nav-icon fas fa-user-lock"></i></font>
              <p>
                LockScreen
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Report</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="reportresult.php">Home</a></li>
              <li class="breadcrumb-item active">Report Result</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-lg-3 col-6">
          <!-- small card -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3><?php echo mysqli_num_rows($qry2); ?></h3>

              <p>Comments</p>
            </div>
            <div class="icon">
              <i class="fas fa-comment"></i>
            </div>
            <a href="comment.php" class="small-box-footer">
              More info <i class="fas fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small card -->
          <div class="small-box bg-warning">
            <div class="inner">
              <h3>
                <?php echo $sumpersen;?>
                <sup style="font-size: 20px">%</sup>
              </h3>

              <p>All Score Rate</p>
            </div>
            <div class="icon">
              <i class="fas fa-chart-bar"></i>
            </div>
            <a href="allscore.php" class="small-box-footer">
              More info <i class="fas fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small card -->
          <div class="small-box bg-primary">
            <div class="inner">
              <h3>Print</h3>

              <p>Report Result</p>
            </div>
            <div class="icon">
              <i class="fas fa-print"></i>
            </div>
            <a href="print.php" target="blank" class="small-box-footer">
              Print Report <i class="fas fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small card -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3>Download</h3>

              <p>Import Excel</p>
            </div>
            <div class="icon">
              <i class="fas fa-download"></i>
            </div>
            <a href="report_excel.php" class="small-box-footer">
              Download Excel <i class="fas fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->


        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Table Report Questionnaire</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>No QB / Name</th>
                    <th>Score</th>
                    <th>Presentation</th>
                    <th>Date Time</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  // error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
                  $no2 = 0;
                  while ($data = mysqli_fetch_array($qry1)) {
                    include 'algoritma.php';
                    ?>
                    <tr>
                      <td><?php echo $no2; ?></td>
                      <td><?php echo $identitas; ?></td>
                      <td><?php echo $hasilkonversi; ?></td>
                      <td><?php echo $hasilpersenkonversi;?>%</td>
                      <td><?php echo $time; ?></td>
                      <td width="8"><a href="detailhasilkuesioner.php?id=<?php echo $id; ?>" class="btn btn-success float-right button-space"><i class='font-setting-4 far fa-eye'></i></a></td>
                    </tr>
                  <?php } ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>No QB / Name</th>
                    <th>Score</th>
                    <th>Presentation</th>
                    <th>Date Time</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
</body>
</html>
