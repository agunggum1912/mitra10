<?php
include 'koneksi.php';
$no2++;
$identitas = $data['identitas'];
$time = date("d-m-Y, H:i:s", strtotime($data['time_steam']));
$id = $data['id'];
$hasil = $data['hasil'];
$total[$no2] = $data['hasil'];
$hasilpersen = $hasil / 5 * 100;
$hasilpersenkonversi = number_format($hasilpersen,1,",",".");
$hasilkonversi = number_format($hasil,1,",",".");

if ($hasilpersenkonversi >= 80 && $hasilpersenkonversi < 101) {
  $ket = "Sangat Baik";
}elseif ($hasilpersenkonversi >= 60 && $hasilpersenkonversi < 80) {
  $ket = "Baik";
}elseif ($hasilpersenkonversi >= 50 && $hasilpersenkonversi < 60) {
  $ket = "Cukup";
}elseif ($hasilpersenkonversi >= 25 && $hasilpersenkonversi < 50) {
  $ket = "Buruk";
}elseif ($hasilpersenkonversi >= 0 && $hasilpersenkonversi < 25) {
  $ket = "Sangat Buruk";
}

?>