<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="gambar/logom10.svg">
  <title>Mitra10</title>
  <!-- css manual -->
  <link rel="stylesheet" type="text/css" href="assets/css/style.css">

  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <?php
  include 'koneksi.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['userlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang Akses, login terlebih dahulu"); location.href="login.php"</script>';
  }
  error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

  $id2 = $_GET['id'];
  $qry2 = mysqli_query($koneksi, "SELECT a.id_judul, b.id, b.judul FROM tb_jawaban AS a LEFT JOIN tb_judul AS b ON a.id_judul=b.id WHERE a.id_pelanggan='$id2' && a.id_judul GROUP BY a.id_judul")or die("Query 2 Salah");
  $qry3 = mysqli_query($koneksi, "SELECT kritik, identitas FROM tb_pelanggan WHERE id='$id2'");
  $row3 = mysqli_fetch_array($qry3);

  $qry4 = mysqli_query ($koneksi,"SELECT * FROM sum_score") or die ("query 2 salah");
  $no = 0;
  $no2 = 0;
  while ($data = mysqli_fetch_array($qry4)) {
    include 'algoritma.php';
  }
  $totalsum = number_format((array_sum($total) / $no2),2,",",".");
  $sumpersen = number_format(((array_sum($total) / $no2) / 5 * 100),2,",",".");

  ?>
  <style type="text/css">
    body{
      font-family: "Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
      -webkit-print-color-adjust: exact;
    }
    .logo {
      height: 150px;
      width: 300px;
      display: block;
      margin-left: auto;
      margin-right: auto;
    }

    .judul {
      display: block;
      text-align: center;
      border-top: 5px double #000;
      border-bottom: 5px double #000;
      padding: 10px 0px;
    }

    .red {
      color: red;
    }
  </style>


</head>

<body>
  <div style="page-break-after:always;">
    <img class="logo" src="gambar/logomitra10.svg">
    <span style="display: block; margin: 0; text-align: center;">Jl. BSD Raya Utama, BSD City, Tangerang</span>
    <span style="display: block; margin: 0;  text-align: center;">Telp (021) 80634588</span></br>

    <h1 class="judul">REPORT DETAIL RESULT QUESTIONNAIRE</h1></br>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">  
        <?php while($row2 = mysqli_fetch_array($qry2)){?>
          <!-- Main row -->
          <div class="row">
            <div class="col-md-12">
              <!-- TABLE: LATEST ORDERS -->
              <div class="card">
                <div class="card-header font-setting-3 bg-color-primary">
                  <h3 class="card-title">Pertanyaan <?php echo $row2['judul'];?></h3>
                  <div></div>
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus font-setting-4"></i>
                    </button>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                  <div class="table-responsive">
                    <table class="table-question m-0">
                      <thead>
                        <tr>
                          <th><center>No</center></th>
                          <th><center>Question</center></th>
                          <th><center>Score</center></th>
                          <th><center>Result</center></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $no = 0;

                        $idjudul = $row2['id'];
                        $sql1 = "SELECT * FROM laporan_detail WHERE id_pelanggan='$id2' && id_judul='$idjudul'";
                        $qry1 = mysqli_query($koneksi, $sql1)or die("Query 1 Salah");
                        while($row1=mysqli_fetch_array($qry1)){ 

                          $no++;
                          ?>
                          <tr>
                            <td width="6.5%"><center><?php echo $no;?></center></td>
                            <td><?php if ($row1['pertanyaan'] != '') {
                              echo $row1['pertanyaan'];
                            }else{
                              echo "<span class='red'>Pertanyaan telah di Edit/Hapus!</span>";
                            }?></td>
                            <td width="6.5%"><center><?php echo $row1['jawaban']; ?></center></td>
                            <td width="13%"><center><?php echo $row1['hasil']; ?></center></td>
                          </tr>
                        <?php }?>

                      </tbody>
                    </table>
                  </div>
                  <!-- /.table-responsive -->
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              <!-- <br> -->
            <?php }?>
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table-question m-0">
                    <thead>
                      <tr>
                        <th><center>Kritik dan Saran</center></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><?php echo $row3['kritik'];?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table-question m-0">
                    <thead>

                      <tr>
                        <th>No QB / Name</th>
                        <th width="12%"><center><?php echo $row3['identitas'];?></center></th>
                      </tr>
                      <tr>
                        <th>Total Presentation</th>
                        <th width="12%"><center><?php echo $sumpersen;?>%</center></th>
                      </tr>
                      <tr>
                        <th>Total Score</th>
                        <th width="12%"><center><?php echo $totalsum;?></center></th>
                      </tr>
                    </thead>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->

    <script>
      window.print();
    </script>

  </div>


  <!-- jQuery -->
  <script src="plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- ChartJS -->
  <script src="plugins/chart.js/Chart.min.js"></script>
  <!-- Sparkline -->
  <script src="plugins/sparklines/sparkline.js"></script>
  <!-- JQVMap -->
  <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
  <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="plugins/moment/moment.min.js"></script>
  <script src="plugins/daterangepicker/daterangepicker.js"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <!-- Summernote -->
  <script src="plugins/summernote/summernote-bs4.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="dist/js/pages/dashboard.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
</body>
</html>
