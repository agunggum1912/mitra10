<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="shortcut icon" type="image/x-icon" href="gambar/logom10.svg">
  <title>Mitra10</title>

  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/style-index.css">
  <link rel="stylesheet" href="assets/font-awesome/css/all.min.css" type="text/css">

  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <style type="text/css">

    .color{
      color: #6c757d;
      font-size: 12pt;
      font-weight: 900;
    }

  </style>
</head>

<?php
include 'koneksi.php';

$qry = mysqli_query($koneksi,"SELECT * FROM tb_judul WHERE id_active=1")or die("Query Judul salah");

// error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
if (isset($_POST['tambah'])) {
  $kritik = $_POST['kritik'];
  $noqb = $_POST['noqb'];

  if (empty($noqb)) {
    echo "<script>alert('Silahkan masukkan No Receipt yang tertera di Struk pembelanjaan atau masukkan Nama anda!');history.go(-1)</script>";
  }elseif (strlen($noqb) > 31 || strlen($noqb) < 2) {
    echo "<script>alert('No Receipt atau Nama harus 2-30 character!');history.go(-1)</script>";
  }elseif (strlen($kritik) > 300) {
    echo "<script>alert('Kritik & Saran maximal 300 panjang karakter!');history.go(-1)</script>";
  }else{
    $sql2 = "INSERT INTO tb_pelanggan(id,kritik,identitas,time_steam) VALUES (NULL,'$kritik','$noqb',current_timestamp())";
    $hasil = mysqli_query($koneksi, $sql2)or die("Query 2 salah!");
  }

  $qry3 = mysqli_query($koneksi,"SELECT * FROM tb_judul WHERE id_active=1")or die("Query 3 salah");
  $sql4 = "SELECT * FROM tb_pelanggan WHERE kritik='$kritik' && identitas='$noqb'";
  $qry4 = mysqli_query($koneksi,$sql4) or die ("Query 4 Salah!");
  $row4 = mysqli_fetch_array($qry4);
  $id1 = $row4['id'];
  
  while ($row3 = mysqli_fetch_array($qry3)){
    $id_judul = $row3['id'];
    $sql5 = "SELECT * FROM tb_pertanyaan WHERE id_judul='$id_judul'";
    $qry5 = mysqli_query($koneksi,$sql5)or die("Query 5 Salah");
    $no = 0;

    while ($row5 = mysqli_fetch_array($qry5)) {
      $no++;
      $id2 = $row5['id_judul'];
      $id3 = $row5['id'];
      $j = $_POST["r".$id2.$no];

      $sql6 = "INSERT INTO tb_jawaban(id,id_pelanggan,id_judul,id_pertanyaan,jawaban,nilai_pembagi) VALUES (NULL,'$id1','$id2','$id3','$j',1)";
      $qry6 = mysqli_query($koneksi,$sql6)or die("Query 5 Salah!");
    }
  }

  if ($qry4) {
    echo "<script>alert('Terimakasih survey anda sangat berharga bagi kami:).') ;window.location='index.php'; </script>";
  }else{
    echo "<script>alert('Opss! Sepertinya ada yang salah!');history.go(-1)</script>";
  }
}

?>

<body>


  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="index.php">
      <img src="gambar/logomitra10.svg" width="100" height="45">
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li><p><h4>KUESIONER TINGKAT KEPUASAN PELANGGAN</h4></p></li>
      </ul>
      <a href="login.php" class="btn btn-outline-primary mr-2">
        <i class="fa fa-sign-in-alt"></i> Login
      </a>
    </div>

  </nav>
  <div class="hidden">Hidden</div>


  <form name="jawaban_proses" method="post" action="">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="offset-md-2 col-md-8">
            <div class="card-index">
              <!-- /.card-header -->
              <div class="card-body">
                <div class="card-header2">
                  <p><h5>Terimakasih telah berbelanja di Mitra10 Q-Big <i class="far fa-smile"></i></h5></p>
                </div>
              Silahkan masukkan No Transaksi anda yang berada di atas struk pembelanjaan</br>
              <i style="padding-left: 200px" class="fas fa-arrow-down"></i> <i class="fas fa-arrow-down"></i> <i class="fas fa-arrow-down"></i></br>
              <table width="100%">
                <tr>
                  <td width="150">Receipt No / Nama :</td>
                  <td><input required minlength="2" maxlength="30" class="textbox1" type="text" name="noqb" autocomplete="off"></td>
                </tr>
              </table>
            </br>
          </br>
          <?php
            $qry7 = mysqli_query($koneksi, "SELECT * FROM kuesioner")or die("Query 7 Salah!");
          while($row7 = mysqli_fetch_array($qry7)){
            $idjudul = $row7['id_judul'];
            if ($row7['judul']) {  
              ?>
              <table class="table table-index">
                <thead>                  
                  <tr>
                    <th style="width: 10px">No</th>
                    <th>Pertanyaan <?php echo $row7['judul'];?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $sql1 = "SELECT * FROM tb_pertanyaan WHERE id_judul='$idjudul'";
                  $qry1 = mysqli_query($koneksi,$sql1) or die ("Query Tabel salah!");
                  $no = 0;
                  while($row1 = mysqli_fetch_array($qry1)){;
                    $no++;
                    ?>
                    <tr>
                      <td align="center" valign="top"><?php echo $no;?></td>
                      <td><span class="color"><?php echo $row1['pertanyaan'];?></span></br></br>
                        <div style="padding-right: 20px;" class="icheck-primary d-inline">
                          <input required type="radio" name="r<?php echo "$idjudul"."$no";?>" id="radioPrimary<?php echo "$idjudul"."$no";?>1" value="5">
                          <label class="color" for="radioPrimary<?php echo "$idjudul"."$no";?>1">
                            Sangat Setuju
                          </label>
                        </div>
                        <div style="padding-right: 20px;" class="icheck-primary d-inline">
                          <input required type="radio" name="r<?php echo "$idjudul"."$no";?>" id="radioPrimary<?php echo "$idjudul"."$no";?>2" value="4">
                          <label class="color" for="radioPrimary<?php echo "$idjudul"."$no";?>2">
                            Setuju
                          </label>
                        </div>
                        <div style="padding-right: 20px;" class="icheck-primary d-inline">
                          <input required type="radio" name="r<?php echo "$idjudul"."$no";?>" id="radioPrimary<?php echo "$idjudul"."$no";?>3" value="3">
                          <label class="color" for="radioPrimary<?php echo "$idjudul"."$no";?>3">
                            Netral
                          </label>
                        </div>
                        <div style="padding-right: 20px;" class="icheck-primary d-inline">
                          <input required type="radio" name="r<?php echo "$idjudul"."$no";?>" id="radioPrimary<?php echo "$idjudul"."$no";?>4" value="2">
                          <label class="color" for="radioPrimary<?php echo "$idjudul"."$no";?>4">
                            Tidak Setuju
                          </label>
                        </div>
                        <div style="padding-right: 20px;" class="icheck-primary d-inline">
                          <input required type="radio" name="r<?php echo "$idjudul"."$no";?>" id="radioPrimary<?php echo "$idjudul"."$no";?>5" value="1">
                          <label class="color" for="radioPrimary<?php echo "$idjudul"."$no";?>5">
                            Sangat Tidak Setuju
                          </label>
                        </div>
                      </br>
                    </br>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </br>
      </br>
      <?php
    }
  }
  ?>

  <span style="color: #6c6c6c">Kritik & Saran!</span>
  <textarea maxlength="300" class="textbox2" type="text" name="kritik" placeholder="Bagaimana pelayanan kita hari ini:)"></textarea>

</div>
<!-- /.card-body -->
<div class="card-footer clearfix">
  <button type="submit" name="tambah" class="btn btn-primary float-right"><i class="fas fa-check"></i> Selesai</button>
</div>
</div>
<!-- /.card -->
</div>
</div>
</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</form>

<!-- /.content-wrapper -->
<footer class="main-footer2">
  <!-- Default to the left -->
  <strong>Copyright &copy; 2020.</strong> All rights reserved.
</footer>


<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<script src="assets/js/jquery.js"></script> 
<script src="assets/js/popper.js"></script> 
<script src="assets/js/bootstrap.js"></script>
</body>
</html>