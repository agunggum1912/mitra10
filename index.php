<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style-gui.css">
    <link rel="stylesheet" href="assets/font-awesome/css/all.min.css" type="text/css">


    <link rel="shortcut icon" type="image/x-icon" href="gambar/logom10.svg">
    <title>Mitra10</title>
</head>


<body>

    <div class="container">
        <div class="background-image"></div>
        
        <div class="tengah kotak_tengah2">
            <center><img class="my-sm-3" src="gambar/logomitra10.svg" width="310"></center>
            <div class="button button-red">
                <a href="kuesioner.php"><i class="fas fa-clipboard-list"></i></br>
                    <font size="6">Kuesioner</font>
                </a>
            </div>
            <div class="button button-blue">
                <a href="login.php"><i class="fas fa-sign-in-alt"></i></br>
                    <font size="6">Login</font>
                </a>
            </div>
        </div>
    </div>


    <script src="assets/js/jquery.js"></script> 
    <script src="assets/js/popper.js"></script> 
    <script src="assets/js/bootstrap.js"></script>
</body>
</html>