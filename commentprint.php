<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php
	include 'koneksi.php';

	// mengaktifkan session
	session_start();
	if (!isset($_SESSION['userlogin'])) {
	// if($_SESSION['status'] != "login") {
		echo '<script language="javascript">alert("Dilarang Akses, login terlebih dahulu"); location.href="login.php"</script>';
	}
	$qry1 = mysqli_query($koneksi,"SELECT kritik, identitas FROM tb_pelanggan")or die("Query 1 salah!");
	$no = 1;

	?>

	<style type="text/css">
		body{
			font-family: "Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
			-webkit-print-color-adjust: exact;
		}
		.logo {
			height: 150px;
			width: 300px;
			display: block;
			margin-left: auto;
			margin-right: auto;
		}

		.judul {
			display: block;
			text-align: center;
			border-top: 5px double #000;
			border-bottom: 5px double #000;
			padding: 10px 0px;
		}

		.tabel {
			border-collapse: collapse;
			width: 100%;
			font-family: sans-serif;
		}

		.tabel thead, th {
			background-color: #00aeef;
			color: #fff;
			border: 2px solid #000;
			padding: 15px 10px;
		}

		.tabel td {
			border: 1px solid #000;
			padding: 5px 10px;
			text-align: center;
		}

		.tabel tr:nth-child(odd){
			background-color: #d7d7d7;
		}

	</style>
</head>
<body>
	<img class="logo" src="gambar/logomitra10.svg">
	<span style="display: block; margin: 0; text-align: center;">Jl. BSD Raya Utama, BSD City, Tangerang</span>
	<span style="display: block; margin: 0;  text-align: center;">Telp (021) 80634588</span></br>

	<h1 class="judul">REPORT KUESIONER</h1>

	<table class="tabel">
		<thead>
			<tr>
				<th>No</th>
				<th>No QB / Name</th>
				<th>Comment</th>
			</tr>
		</thead>
		<tbody>
			<?php while ($row1 = mysqli_fetch_array($qry1)) {?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $row1['identitas']; ?></td>
					<td><?php echo $row1['kritik']; ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>

	<script>
		window.print();
	</script>

</body>
</html>