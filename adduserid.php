<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="gambar/logom10.svg">
  <title>Mitra10</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- css manual -->
  <link rel="stylesheet" type="text/css" href="assets/css/style.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <?php
  include 'koneksi.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['userlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang Akses, login terlebih dahulu"); location.href="login.php"</script>';
  }

  $strSQL = "SELECT tb_user.username, tb_user.nama, tb_user.foto FROM tb_user WHERE username='$_SESSION[userlogin]' ";
  $query = mysqli_query ($koneksi, $strSQL) or die ("query salah");
  $row = mysqli_fetch_array($query);

    if (isset($_POST['submit'])) {
      $nama = trim($_POST['nama']);
      $no_hp = trim($_POST['no_hp']);
      $email = trim($_POST['email']);
      $username = trim($_POST['username']);
      $position = trim($_POST['position']);
      $password = ($_POST['password']);
      $password2 = ($_POST['password2']);
      $password3 = (md5($password));
      $jenis_kelamin = trim($_POST['jenis_kelamin']);
      $tgl_lahir = ($_POST['tgl_lahir']);

      function ubahTanggal($tgl_lahir){
        $pisah = explode('/',$tgl_lahir);
        $array = array($pisah[2],$pisah[1],$pisah[0]);
        $satukan = implode('-',$array);
        return $satukan;
      }

      $waktu = ubahTanggal($tgl_lahir);

      $alfabet = preg_match('@[a-zA-Z]@', $password);
      $number = preg_match('@[0-9]@', $password);
      $lowercase = preg_match('@[a-z]@', $username);
      $number2 = preg_match('@[0-9]@', $username);

      $ekstensi_diperbolehkan = array('png','jpg','jpeg');
      $nmfoto = $_FILES['foto']['name'];
      $x = explode('.', $nmfoto);
      $ekstensi = strtolower(end($x));
      $ukuran = $_FILES['foto']['size'];
      $file_tmp = $_FILES['foto']['tmp_name']; 

      $cek = "SELECT username FROM tb_user";
      $cekquery = mysqli_query($koneksi, $cek);
      while ($cekrow = mysqli_fetch_array($cekquery)) {
        $cekusername = $cekrow['username'];

        if ($cekusername == $username) {
          echo "<script>alert('Username sudah ada !');history.go(-1)</script>";
        }
      }



      if (empty($nama) && empty($no_hp) && empty($tgl_lahir) && empty($username) && empty($position) && empty($password) && empty($password) && empty($password2)) {
        echo "<script>alert('Data masih kosong!');history.go(-1)</script>";
      }elseif (empty($nama)) {
        echo "<script>alert('Name harus di isi!');history.go(-1)</script>";
      }elseif (empty($no_hp)) {
        echo "<script>alert('Number Phone harus di isi!');history.go(-1)</script>";
      }elseif (empty($tgl_lahir)) {
        echo "<script>alert('Date of Birth harus di isi!');history.go(-1)</script>";
      }elseif (empty($username)) {
        echo "<script>alert('Username harus di isi!');history.go(-1)</script>";
      }elseif(empty($email)){
        echo "<script>alert('Email harus di isi!');history.go(-1)</script>";
      }elseif (empty($position)) {
        echo "<script>alert('Silahkan pilih Position!');history.go(-1)</script>";
      }elseif (empty($password)) {
        echo "<script>alert('Password harus di isi!');history.go(-1)</script>";
      }elseif (empty($jenis_kelamin)) {
        echo "<script>alert('Silahkan pilih Gender!');history.go(-1)</script>";
      }elseif (empty($password2)) {
        echo "<script>alert('Confirm Password harus di isi!');history.go(-1)</script>";
      }elseif (!preg_match("/^[a-zA-Z ]*$/", $nama)) {
        echo "<script>alert('Name tidak boleh menganduk special char dan angka!');history.go(-1)</script>";
      }elseif (strlen($nama) >= 33) {
        echo "<script>alert('Panjang Name tidak boleh lebih dari 32 Character!');history.go(-1)</script>";
      }elseif (!preg_match("/^[0-9]*$/", $no_hp)) {
        echo "<script>alert('Number Phone tidak boleh mengandung special character atau huruf!');history.go(-1)</script>";
      }elseif (strlen($no_hp) <= 9 || strlen($no_hp) >= 14) {
        echo "<script>alert('Number Phone hanya boleh 10-13 Angka!');history.go(-1)</script>";
    // }elseif (checkdate($v_tgl[1], $v_tgl[0], $v_tgl[2])) {
    //   echo "<script>alert('Date of Birth yang anda masukkan salah!');history.go(-1)</script>";
      }elseif (!$lowercase || !$number2 || strlen($username) <= 7 || strlen($username) >= 13) {
        echo "<script>alert('Username harus mengandung angka dan huruf, tidak boleh special character, huruf Capital & 8-12 panjang character!');history.go(-1)</script>";
      }elseif (strlen($email) >= 61) {
        echo "<script>alert('Panjang Email tidak boleh lebih dari 60 Character!');history.go(-1)</script>";
      }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo "<script>alert('Format email yang anda masukkan salah!');history.go(-1)</script>";
      }elseif (!$alfabet ||  !$number || strlen($password) <= 5) {
        echo "<script>alert('Password  harus mengandung angka dan huruf, tidak boleh special character & lebih 6 panjang character!');history.go(-1)</script>";
      }elseif ($password != $password2) {
        echo "<script>alert('Confirm Password tidak sama!');history.go(-1)</script>";
      }elseif ($position != "Administration" && $position != "Head of Division" && $position != "Human Resource Departement" && $position != "Manager On Duty" && $position != "Regional Manager" && $position != "Shared Service Center" && $position != "Store Manager") {
        echo "<script>alert('Position Salah!');history.go(-1)</script>";
      }elseif ($jenis_kelamin != "Female" && $jenis_kelamin != "Male") {
        echo "<script>alert('Gender Salah!');history.go(-1)</script>";
      }elseif(in_array($ekstensi, $ekstensi_diperbolehkan) === true){
        if($ukuran < 1044070){
          $nmfoto = date("YmdHis").$nmfoto;
          move_uploaded_file($file_tmp, 'foto/'.$nmfoto);

          $sql = "INSERT INTO tb_user (id,level,username,email,password,nama,no_hp,tgl_lahir,jenis_kelamin,foto,tgl_daftar) VALUES (NULL,'$position','$username','$email','$password3','$nama','$no_hp','$waktu','$jenis_kelamin','$nmfoto',current_timestamp())";
          $sql2 = mysqli_query($koneksi, $sql) or die ("query insert salah");

          if($sql2){
            echo "<script>alert('Telah berhasil di tambahkan.');window.location='edituserid.php'; </script>";
          }else{
            echo "<script>alert('GAGAL!'); </script>";
          }
        }else{
          echo "<script>alert('Ukuran File Terlalu Besar!');</script>";
        }
      }else{
        echo "<script>alert('Ekstensi File yang di Upload tidak di perbolehkan!');</script>";
      }
    }

    ?>


  </head>
  <body class="hold-transition sidebar-mini">
    <div class="wrapper">
      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
          </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
              <?php echo $row["username"];?> 
              <i class="fas fa-user-alt"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <div class="dropdown-divider"></div>
              <a href="setting.php" class="dropdown-item">
                <i class="fas fa-cog mr-2"></i>
                <span class="float-right text-muted text-sm">Setting</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="logout.php" class="dropdown-item">
                <i class="fas fa-sign-out-alt mr-2"></i>
                <span class="float-right text-muted text-sm">Logout</span>
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <!-- /.navbar -->

      <!-- Main Sidebar Container -->
      <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="reportresult.php" class="brand-link">
          <img src="gambar/logom10.svg" alt="AdminLTE Logo" class="brand-image elevation-3"
          style="opacity: .8">
          <span class="brand-text font-weight-light">Mitra10 Q-Big</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
          <!-- Sidebar user panel (optional) -->
          <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
              <?php
              $cek_foto = $row ['foto'];
              $tempat_foto = 'foto/'.$row['foto']; 
              if ($cek_foto) {
                echo "<img src='$tempat_foto' class='img-circle elevation-2' alt='User Image'>"; 
              }else{
                echo "<img src='foto/blank.png'></a>";
              }
              ?>
            </div>
            <div class="info">
              <a href="setting.php" class="d-block">
                <?php echo $row["nama"];?></a>
              </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
           <li class="nav-item">
            <a href="reportresult.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Report Result
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview  menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Manage User Id
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="adduserid.php" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add User Id</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="updateuserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Update User Id</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Manage Question
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="title.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Question Title</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="question.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Question</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="setting.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="lockscreen.php?username=<?php echo $_SESSION['userlogin']; ?>" class="nav-link">
              <font style="color: #ed1c24;"><i class="nav-icon fas fa-user-lock"></i></font>
              <p>
                LockScreen
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add User Id</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="reportresult.php">Home</a></li>
              <li class="breadcrumb-item"><a href="adduserid.php">Manage User Id</a></li>
              <li class="breadcrumb-item active">Add User Id</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid col-10">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">New User Id</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <form action="" name="submit" method="post" enctype="multipart/form-data">
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Name</label>
                    <input required name="nama" type="text" class="form-control">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Number Phone</label>
                    <div class="input-group">
                      <input required minlength="11" maxlength="14" name="no_hp" type="text" class="form-control">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-phone"></i></span>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Date of Birth</label>
                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                      <input required name="tgl_lahir" type="text" class="form-control datetimepicker-input" data-target="#reservationdate" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                      <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Username</label>
                    <input required name="username" minlength="6" maxlength="12" type="text" class="form-control">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->  
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Email</label>
                    <input required name="email" maxlength="60" type="text" class="form-control">
                  </div>
                </div>
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-sm-6">
                  <!-- text input -->
                  <div class="form-group">
                    <label>Password</label>
                    <input required name="password" minlength="6" type="Password" class="form-control">
                  </div>
                </div>
                <div class="col-sm-6">
                  <!-- text input -->
                  <div class="form-group">
                    <label>Confirm Password</label>
                    <input required name="password2" minlength="6" type="Password" class="form-control">
                  </div>
                </div>
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Gender</label>
                    <select required name="jenis_kelamin" class="form-control">
                      <option></option>
                      <option value="Female">Female</option>
                      <option value="Male">Male</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Position</label>
                    <select name="position" required class="form-control">
                      <option></option>
                      <option value="Administration">Administration</option>
                      <option value="Head of Division">Head of Division</option>
                      <option value="Human Resource Departement">Human Resource Departement</option>
                      <option value="Manager On Duty">Manager On Duty</option>
                      <option value="Regional Manager">Regional Manager</option>
                      <option value="Shared Service Center">Shared Service Center</option>
                      <option value="Store Manager">Store Manager</option>
                    </select>
                  </div>
                </div>
              </div>
              <!-- ./row -->
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <!-- Uploaded image area-->
                    <label for="exampleInputFile">Profil Picture</label>
                    <div class="image-area"><img id="imageResult" src="#" alt="" class="img-fluid rounded shadow-sm mx-auto d-block"></div>
                    <div class="input-group border1 mt-4 mb-3 bg-white">
                      <!-- <div class="input-group mb-3 px-2 py-2 rounded-pill bg-white shadow-sm"> -->
                        <input required id="upload" type="file" name="foto" onchange="readURL(this);" class="form-control border-0" >
                        <label id="upload-label" for="upload" class="font-weight-light text-muted">Choose file</label>
                        <div class="input-group-append">
                          <label for="upload" class="btn btn-abu m-0"> <i class="fa fa-cloud-upload text-muted"></i><small class="text-uppercase font-weight-bold text-muted">Choose file</small></label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- ./row -->
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button onclick="return confirm('Apakah User Id yang anda buat sudah benar?')" name="submit" type="submit" class="btn btn-primary float-right">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
        <!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
      <strong>Copyright &copy; 2020 <!-- <a href="http://adminlte.io">AdminLTE.io</a> -->.</strong>
      All rights reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Select2 -->
  <script src="plugins/select2/js/select2.full.min.js"></script>
  <!-- Bootstrap4 Duallistbox -->
  <script src="plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
  <!-- InputMask -->
  <script src="plugins/moment/moment.min.js"></script>
  <script src="plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
  <!-- date-range-picker -->
  <script src="plugins/daterangepicker/daterangepicker.js"></script>
  <!-- bootstrap color picker -->
  <script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <!-- Bootstrap Switch -->
  <script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
  <!-- Page script -->
  <script>
    $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent: false
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#imageResult')
          .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
      }
    }

    $(function () {
      $('#upload').on('change', function () {
        readURL(input);
      });
    });

    var input = document.getElementById( 'upload' );
    var infoArea = document.getElementById( 'upload-label' );

    input.addEventListener( 'change', showFileName );
    function showFileName( event ) {
      var input = event.srcElement;
      var fileName = input.files[0].name;
      infoArea.textContent = 'File name: ' + fileName;
    }
  </script>
</body>
</html>
