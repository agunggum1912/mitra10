<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php
	include 'koneksi.php';

	// mengaktifkan session
	session_start();
	if (!isset($_SESSION['userlogin'])) {
	// if($_SESSION['status'] != "login") {
		echo '<script language="javascript">alert("Dilarang Akses, login terlebih dahulu"); location.href="login.php"</script>';
	}

	$sql1 = "SELECT * FROM sum_score";
	$qry1 = mysqli_query ($koneksi, $sql1) or die ("query 1 salah");
	$no2 = 0;
	?>

	<style type="text/css">
		body{
			font-family: "Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
			-webkit-print-color-adjust: exact;
		}
		.logo {
			height: 150px;
			width: 300px;
			display: block;
			margin-left: auto;
			margin-right: auto;
		}

		.judul {
			display: block;
			text-align: center;
			border-top: 5px double #000;
			border-bottom: 5px double #000;
			padding: 10px 0px;
		}

		.tabel {
			border-collapse: collapse;
			width: 100%;
			font-family: sans-serif;
		}

		.tabel thead, th {
			background-color: #00aeef;
			color: #fff;
			border: 2px solid #000;
			padding: 15px 10px;
		}

		.tabel td {
			border: 1px solid #000;
			padding: 5px 10px;
			text-align: center;
		}

		.tabel tr:nth-child(odd){
			background-color: #d7d7d7;
		}

	</style>
</head>
<body>
	<?php
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=Score Presentation Questionnaire.xls");
	?>

	<table>
		<tr>
			<td colspan="5">
				<h1 class="judul">REPORT KUESIONER</h1>		
			</td>
		</tr>
	</table>
	<br>

	<table class="tabel">
		<thead>
			<tr>
				<th><h4>No</h4></th>
				<th><h4>No QB / Name</h4></th>
				<th><h4>Score</h4></th>
				<th><h4>Presentation</h4></th>
				<th><h4>Result Information</h4></th>
			</tr>
		</thead>
		<tbody>
			<?php
			while ($data = mysqli_fetch_array($qry1)) {
				include 'algoritma.php';
				?>
				<tr>
					<td><?php echo $no2; ?></td>
					<td><?php echo $identitas; ?></td>
					<td><?php echo $hasilkonversi; ?></td>
					<td><?php echo $hasilpersenkonversi;?>%</td>
					<td><?php echo $ket ?></td>
				</tr>
				<?php
			}
			$totalsum = number_format((array_sum($total) / $no2),2,",",".");
			$sumpersen = number_format(((array_sum($total) / $no2) / 5 * 100),2,",",".");
			?>
		</tbody>
		<tfoot>
			<tr>
				<th colspan="2"><h4>Total All Averrage</h4></th>
				<th><h4><?php echo $totalsum;?></h4></th>
				<th><h4><?php echo $sumpersen;?>%</h4></th>
				<th><h4>Result Information</h4></th>
			</tr>
		</tr>
	</tfoot>
</table>


</body>
</html>